import os
import sys
import json
import logging

from urllib import parse
from flask import Flask, Response, request

from db import mssql_proxy, mysql_proxy, db_proxy

app = Flask(__name__)

@app.route('/', methods=['POST'])
def sql_proxy_cmd():
    request_json = request.json

    scheme = parse.urlparse(request_json['connection_string']).scheme
    
    if scheme == 'mssql':
        proxy = mssql_proxy.MsSqlProxy(request_json['connection_string'], request_json['sql'])
    elif scheme == 'mysql':
        proxy = mysql_proxy.MySqlProxy(request_json['connection_string'], request_json['sql'])
    else:
        return Response("Unsupported database", status=400, mimetype='application/text')

    try:
        proxy.connect()
        result = proxy.execute_query()
        sys.stdout.flush()
    except Exception as e:
        logging.error(e)
        return Response("Error during processing", status=500, mimetype='application/text')
    finally:
        proxy.close()

    if result is False:
        return Response("Error during processing", status=500, mimetype='application/text')
    else:
        return json.dumps({'result': result})

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))