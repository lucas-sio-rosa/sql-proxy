import logging
import pymysql
from pymysql import cursors

from db.db_proxy import DatabaseProxy

class MySqlProxy(DatabaseProxy):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def connect(self):
        self.conn = pymysql.connect(
            host=self.parsed_conn.hostname,
            user=self.parsed_conn.username,
            password=self.parsed_conn.password,
            db=self.parsed_conn.path[1:],
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )

        return True

    def execute_query(self):
        cursor = self.conn.cursor()
        try:
            cursor.execute(self.sql)
            self.conn.commit()
        except Exception as e:
            logging.error(e)
            cursor.close()
            return False

        try:
            return [*cursor.fetchone().values()][0]
        except Exception as e:
            logging.error(e)
            return True
        finally:
            cursor.close()

    def close(self):
        self.conn.close()