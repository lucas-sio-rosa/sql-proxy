import logging
import pyodbc

from db.db_proxy import DatabaseProxy

class MsSqlProxy(DatabaseProxy):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def connect(self):
        conn_str = 'Driver={{ODBC Driver 17 for SQL Server}};Server={};Database={};UID={};PWD={}'.format(
            self.parsed_conn.hostname,
            self.parsed_conn.path[1:],
            self.parsed_conn.username,
            self.parsed_conn.password
        )

        self.conn = pyodbc.connect(conn_str, timeout=30, autocommit=True)
        return True

    def execute_query(self):
        cursor = self.conn.cursor()
        try:
            cursor.execute(self.sql)
        except Exception as e:
            logging.error(e)
            return False

        try:
            return cursor.fetchone()[0]
        except Exception as e:
            return True
        finally:
            cursor.close()
    
    def close(self):
        self.conn.close()