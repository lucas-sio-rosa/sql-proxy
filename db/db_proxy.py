from abc import ABC, abstractmethod
from urllib import parse

class DatabaseProxy(ABC):
    def __init__(self, connection_string, sql):
        self.connection_string = connection_string
        self.sql = sql
        self.parsed_conn = parse.urlparse(connection_string)
        self.conn = None
    
    @abstractmethod
    def connect(self):
        '''This method should connect to the database and return an object to be later used to run queries'''
        pass

    @abstractmethod
    def execute_query(self):
        '''This method should take the connection, run the statement and return the value of the first cell in the first row.
        In case no data was returned, the method should simply return true'''
        pass

    @abstractmethod
    def close(self):
        '''This method should close the database connection'''
        pass