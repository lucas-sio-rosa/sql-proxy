# DB Proxy

This project aims to be a simple proxy for databases where a single SQL query is sent, executed and the first cell returned to the caller.
This allows for 2 specific scenarios:
* Probing: a query returns a single value with which you can decide whether or not to proceed with the rest of your pipeline (i.e. an Airflow pipeline that might not always have fresh data to be loaded)
* Proxy: a query that does something internally on the database (i.e. creating temporary tables), where the only interest is knowing whether or not the statement executed successfully

Even more specifically, this project came to existance when used in conjunction with GCP Cloud Composer, where the nodes are Kubernetes pods.
Here, if you have a VPN to on-prem resources, a tunnel will be required to the internal cluster's IP range, which changes if you recreate your environment.
With that in mind, this container could be deployed to Cloud Run, where you can configure a Serverless VPC Access, allowing you to have a single IP range accessing the on-prem resources and freeing up
the Composer cluster. And, by ensuring that only authorized calls are accepted, the risk of abuse is mitigated.
While the use cases may be very specific, the code is designed to be as simple and direct as possible, to emphasize the simplicity of its core intentions.

## Setup

The project is a container and can be built with docker. If GCP Container Registry is used, the image can be built with:
```sh
gcloud builds submit --tag gcr.io/${PROJECT}/sql-proxy --project ${PROJECT}
```

As stated previously, the easiest way to deploy it is to make use of GCP Cloud Run. If making use of GCP Container Registry, it can be deployed by:

```sh
gcloud beta run deploy sql-proxy --image gcr.io/${PROJECT}/sql-proxy --platform managed --project ${PROJECT} --region ${REGION} --vpc-connector ${VPC}
```

The VPC is essential here: with it the deployment will be able to access resources by their internal IP (even on-prem, as long as a tunnel has been configured).
Other options are avaiable to control aspects such as ports, memory and CPU allocation and more.

## Usage

The root URL is a POST call, that expects 2 parameters:
* The connection string which points to the database
* The SQL query to be executed

The connection string is parsed with urllib, and generally follows the web2py pattern, such as: `mssql://USER:PWD@HOST/DB`. The scheme will decide if the informed connection string is supported.
While no advanced connection options are supported currently, remember that the use cases are meant to be simple (extracting data or running long procedures are not the idea here).
For the SQL query, no checks are made to prevent abuse, so ensure that the credentials do not have high privileges on the databases. Furthermore, the fact that authentication is required to call the function and that only the first cell is returned, should inhibt major exploits, but it's left to the DBAs to try to further block unwanted acesses.

For testing, the service can be called with CURL:

```sh
curl https://${URL} -H "Authorization: bearer $(gcloud auth print-identity-token)" -H "Content-Type:application/json" -d '{"connection_string": "${CONNECTION_STRING}", "sql": "${SQL}"}'
```

Also, in line with the idea to further inhibt misuse, any errors encountered during execution return a generic 500 error with details being logged internally, so that outsider users do not have
too much information to infer anything about the databases being queried. Once again, if deployed on GCP Cloud Run, logs can be seen through Stackdriver.

## Databases

Currently supported:
* SQL Server
* MySQL (without GCP SQL Proxy)

## Author

* [**Lucas Rosa**](https://bitbucket.org/dotz-lucas-rosa/)
